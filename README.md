Dr. Bruce Chau and Dr. Barak Tanzman take a personalized approach to each and every patient. Every consultation is tailored to meeting the personal goals of our patients, deriving from over 30 years of experience in the field of cosmetic and reconstructive surgery.

Address: 27901 Woodward Ave, #100, Berkley, MI 48072

Phone: 248-799-2880
